package com.example.rot13;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RotApplication {
    public static void main(String[] args){
        SpringApplication.run(RotApplication.class,args);
    }
}
