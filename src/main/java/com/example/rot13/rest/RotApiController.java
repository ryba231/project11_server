package com.example.rot13.rest;

import com.example.rot13.dto.RotDto;
import com.example.rot13.service.impl.RotServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "/api")
public class RotApiController {


    private final RotServiceImpl rotService;

    @Autowired
    public RotApiController(RotServiceImpl rotService){
        this.rotService = rotService;
    }

    @RequestMapping(value = "/rot13/encode",method = RequestMethod.POST)
    public ResponseEntity<RotDto> requestBody(@RequestBody RotDto rotDto){
        return new ResponseEntity<>(rotService.getRot(rotDto.getResult()), HttpStatus.OK);
    }


    @RequestMapping(value = "/rot13/decode",method = RequestMethod.POST)
    public ResponseEntity<RotDto> answerDecode(@RequestBody RotDto rotDto){
        return new ResponseEntity<>(rotService.getRot(rotDto.getResult()),HttpStatus.OK);
    }

}
