package com.example.rot13.dto;

public class RotDto {

    private String result;

    public RotDto(){

    }

    public RotDto(String result){
        this.result = result;
    }

    public String getResult() {
        return result;
    }
}
