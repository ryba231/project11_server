package com.example.rot13.service;

import com.example.rot13.dto.RotDto;

public interface RotService {

RotDto getRot(String text);

}
