package com.example.rot13.service.impl;

import com.example.rot13.dto.RotDto;
import com.example.rot13.service.RotService;
import org.springframework.stereotype.Service;

@Service
public class RotServiceImpl implements RotService {

    public RotServiceImpl(){}

    @Override
    public RotDto getRot(String text){
        StringBuilder inscription = new StringBuilder();   // klasa która umożliwia później utworzenia stringa

        for(int i=0;i<text.length();i++){
            char symbol = text.charAt(i);   //rozbijanie Stringa na pojedyncze wartości
            if(symbol >= 'a' && symbol <= 'm'){
                symbol +=13;
            }
            else if(symbol >= 'A' && symbol <= 'M'){
                symbol +=13;
            }
            else if(symbol >= 'n' && symbol <= 'z'){  // przeszukuje text i szyfruje
                symbol -=13;
            }
            else if(symbol >= 'N' && symbol <= 'Z'){
                symbol -=13;
            }
            inscription.append(symbol); //metoda dodająca symbole do ciągu znaków
        }

        return new RotDto(inscription.toString()); //zwraca jednocześnie łącząc pojedyncze symbole w jednego Stringa
    }


}
